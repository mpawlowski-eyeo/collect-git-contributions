# Collect Git contributions

Collects commits made by a user from a Git repository into a spreadsheet, in order to create a registry of authored contributions.

## Assumptions

Repositories are cloned from gitlab.com or a self-hosted gitlab instance.

We only care about commits that were submitted to the remote, local branches are not considered contributions.

## Requirements

Python modules defined in requirements.txt.

You can install them via

```
pip install -r requirements.txt
```

## Usage

```
python collect-git-contributions.py ./path/to/my/repo --user=my.name@org.com --since="January 1st 2022"
```

It makes sense to pass the start date of your employment as `--since`.

If you contribute to multiple repositories, you can pass several:
```
python collect-git-contributions.py ~/dev/abpchromium/src/ ~/dev/libadblockplus/
```

## Output

An XLSX spreadsheet file that can be imported to Google Sheets or just sent to whoever wants to see your contributions.
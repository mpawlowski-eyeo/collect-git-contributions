#!/bin/python

import argparse
import git
import re
import xlsxwriter


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Collect my contributions from git')
    parser.add_argument('paths', metavar='REPO_PATH', nargs='+',
                        help='path to the git repository')
    parser.add_argument('--author', default="m.pawlowski@eyeo.com",
                        help='email author of commits')
    parser.add_argument('--since', default="June 1st 2020",
                        help='start collecting commits authored after this date')
    parser.add_argument('--ref_pattern', default=".*",
                        help='Perl regex to match searched branches. It is okay to search all branches')
    parser.add_argument('--output', default="contributions.xlsx",
                        help='path to output XLSX file')
    return parser.parse_args()


def get_remote_url(repo):
    # Assume we care only about origin
    # Assume there's only one URL defined for the remote
    for url in repo.remote().urls:
        return url


def get_repository_name(repo):
    return get_remote_url(repo).rsplit('/', 1)[1]


def get_link_to_commit(repo, commit):
    base_url = get_remote_url(repo)
    # Assuming the repository was cloned from gitlab using the ssh method.
    # So the URL looks like:
    # git@gitlab.com:mpawlowski-eyeo/collect-git-contributions.git
    # We want to convert it to:
    # https://gitlab.com/mpawlowski-eyeo/collect-git-contributions
    # And then link directly to the commit by adding /commit/asdfasdf123
    match = re.match('git@([\w\.]+):([\w/]+)\.git', base_url)
    if match:
        return 'https://{}/{}/commit/{}'.format(match.group(1), match.group(2), commit.hexsha)


def main():
    args = parse_arguments()
    with xlsxwriter.Workbook(args.output) as workbook:
        # There may be multiple repository paths passed through args.
        # Process all of them.
        for path in args.paths:
            repo = git.Repo(path)
            print('Fetching from remote ' + get_repository_name(repo))
            repo.remote().fetch()

            # Get all remote branches and tags, filter them through
            # the regex pattern provided in args.
            remote_refs = [x for x in repo.remote(
            ).refs if re.findall(args.ref_pattern, x.name)]

            # Create one sheet per repository.
            worksheet = workbook.add_worksheet(
                'Repository ' + get_repository_name(repo))

            # First row becomes column description
            worksheet.write_row(
                0, 0, ['Authored date', 'Description', 'Lines of code changed', 'Link'])

            print('Iterating through commit history')
            # Traverse all commits, filtered by author and date, and insert them into
            # the sheet. We're iterating through a tree, there won't be repeated commits
            # reachable from many branches etc. Commits will be roughly ordered by time.
            for idx, commit in enumerate(repo.iter_commits(remote_refs, author=args.author, since=args.since, no_merges=True)):
                worksheet.write_row(idx + 1, 0, [commit.authored_datetime.strftime(
                    '%d/%m/%y %H:%M:%S'), commit.message, commit.stats.total['lines'], get_link_to_commit(repo, commit)])


if __name__ == "__main__":
    main()
